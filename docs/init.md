# Project initialization

In order to create a new project in your current directory, just run `pilotis init` in a terminal. If you want to create it in another directory, you can also run `pilotis init --project-parent-dir my/other/directory/path`.

Pilotis will ask you for your desired project name. It will then create your project directories organization for both the code and the data, and it will set up your project virtual environment.

What you will get at the end of this command is:

1. A directory with your project source code, divided between `domain`, `application` and `infrastructure` code
1. A `tests` directory
1. A `notebooks` directory
1. A `pyproject.toml` file with your Poetry environment configuration, containing some useful libraries for your project life cycle. Among them, the [_pilotis-io_ library](https://gitlab.com/ekinox-io/ekinox-libraries/pilotis-io) will allow you to read from and write to multiple storage locations while always keeping the same simple API.
1. A `Readme.md` file for onboarding new people on your project
1. A `mkdocs.yml` file and a `docs` directory, with an initial documentation website documenting the tools and conventions used in the bootstrapped project
1. A `.flake8` file with linter conventions
1. A `containers` directory with some docker files for facilitating your deployments
1. A `workdir` directory with your local data
1. Some `makefiles` with some useful commands (run `make --help` to know more about that)

Your project structure should then look like this:
```
containers
makefiles
python
|_ docs
|_ notebooks
|_ tests
|_ <package_name>
|_ .flake8
|_ Makefile
|_ mkdocs.yml
|_ pyproject.toml
scripts
workdir
|_ landing
| |_ raw
| |_ parsed
|_ use_cases
Makefile
README.md  
```