# Cloud computing and storage

| Cloud provider        | Computing           | Storage  |
| ------------- |:-------------:| -----:|
| AWS      | ❌ | ✅ |
| Azure      | ❌  |  ❌ |
| GCP | ❌   |  ❌ |

In order to store your data on the cloud, you can run `pilotis aws` in your project directory or `pilotis aws --project-dir your/project/dir` to create the corresponding infrastructure on AWS. 

Pilotis will then ask you the name of the bucket in which you want to store your data, as well as the one in which you want to store your terraform metadata. It will then use terraform to create your AWS infrastructure.

You will also get access to the `make sync-raw-data-local-to-s3` and `make sync-raw-data-s3-to-local` commands, enabling you to synchronize your data between your local work station and your s3 infrastructure.

Finally, Pilotis will add the [_pilotis-io-aws_ library](https://gitlab.com/ekinox-io/ekinox-libraries/pilotis-io-aws) to your environment, enabling you to read and write data stored in AWS with the same API you use for local data with `pilotis-io`.
