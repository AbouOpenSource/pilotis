from os import mkdir
from subprocess import call, check_output

from hamcrest import assert_that, is_, empty, contains_string
from pytest import mark

from pilotis.commands.git import GitProvider, generate, GITLAB_CI_FILE_NAME, OPTION_PROVIDER, \
    OPTION_GITLAB_GROUP, OPTION_ORGANIZATION
from pilotis.commands.init import generate as generate_init, OPTION_PROJECT_SLUG, OPTION_PYTHON_PACKAGE_NAME, \
    OPTION_PROJECT_NAME
from pilotis.commands.shell_utils import call_shell
from tests.pilotis.test_tools import fake_pilotis_initialization, Capturing

TEST_PROJECT_NAME = "pilotis test project"
TEST_PROJECT_SLUG = "pilotis-test-project"
TEST_PROJECT_PACKAGE_NAME = "pilotis_test_project"
GREP_MATCHING_RETURN_CODE = 0
GREP_NO_MATCHING_RETURN_CODE = 1
MAKE_RETURN_CODE_OK = 0
GITLAB_DATA = {
    OPTION_PROJECT_SLUG: TEST_PROJECT_SLUG,
    OPTION_PROVIDER: 'gitlab',
    OPTION_ORGANIZATION: 'my-organization',
    OPTION_GITLAB_GROUP: "my-group",
}
GITHUB_DATA = {
    OPTION_PROJECT_SLUG: TEST_PROJECT_SLUG,
    OPTION_PROVIDER: 'github',
    OPTION_GITLAB_GROUP: None,
    OPTION_ORGANIZATION: "my-organization",
}


@mark.parametrize("git_provider, git_data",
                  [(GitProvider.GITLAB.value, GITLAB_DATA),
                   (GitProvider.GITHUB.value, GITHUB_DATA)])
def test_add_git_with_any_provider_should_replace_all_placeholders(tmp_path, git_provider, git_data):
    # Given
    project_path = tmp_path / TEST_PROJECT_SLUG
    mkdir(project_path)
    fake_pilotis_initialization(project_path)

    # When
    generate(project_path,
             substitution_data=git_data,
             skip_install=True)

    # Then
    assert_that(call(f"grep -rIl {OPTION_PROJECT_SLUG} {project_path}", shell=True),
                is_(GREP_NO_MATCHING_RETURN_CODE),
                "jinja project slug tags should be replace with given value")
    assert_that(call(f"grep -rIl {TEST_PROJECT_SLUG} {project_path}", shell=True),
                is_(GREP_MATCHING_RETURN_CODE),
                "project name should be used to initialize git repository")


@mark.parametrize("git_provider, git_data",
                  [(GitProvider.GITLAB.value, GITLAB_DATA),
                   (GitProvider.GITHUB.value, GITHUB_DATA)])
def test_generate_git_should_display_help_on_documentation_hosting(tmp_path, git_provider, git_data):
    # Given
    project_path = tmp_path / TEST_PROJECT_SLUG
    mkdir(project_path)
    fake_pilotis_initialization(project_path)

    # When
    with Capturing() as output:
        generate(project_path,
                 substitution_data=git_data,
                 skip_install=True)

    # Then
    assert_that(''.join(output), contains_string("Documentation hosting is not implemented yet."))
    assert_that(''.join(output), contains_string("You may find documentation on how to do it on http"))


@mark.parametrize("git_provider, git_data",
                  [(GitProvider.GITLAB.value, GITLAB_DATA),
                   (GitProvider.GITHUB.value, GITHUB_DATA)])
def test_add_git_with_any_provider_should_be_able_to_initialize_git_repo(tmp_path, git_provider, git_data):
    # Given
    project_path = tmp_path / TEST_PROJECT_SLUG
    mkdir(project_path)
    fake_pilotis_initialization(project_path)

    # When
    generate(project_path,
             substitution_data=git_data,
             skip_install=True)

    # Then
    project_dir = tmp_path / TEST_PROJECT_SLUG
    assert_that((project_dir / ".gitignore").exists(),
                "init should add initialize gitignore")
    assert_that((project_dir / ".git").is_dir())
    git_status_output = check_output("git status -z --porcelain=v1", cwd=project_dir, shell=True)
    assert_that(git_status_output, empty(), "all files should be committed")


@mark.parametrize("git_provider, git_data",
                  [(GitProvider.GITLAB.value, GITLAB_DATA),
                   (GitProvider.GITHUB.value, GITHUB_DATA)])
def test_add_git_with_any_provider_should_add_specific_make_targets_for_git(tmp_path, git_provider, git_data):
    # Given
    init_data = {OPTION_PROJECT_NAME: TEST_PROJECT_NAME,
                 OPTION_PROJECT_SLUG: TEST_PROJECT_SLUG,
                 OPTION_PYTHON_PACKAGE_NAME: TEST_PROJECT_PACKAGE_NAME}
    project_path = tmp_path / TEST_PROJECT_SLUG
    mkdir(project_path)

    # When
    generate_init(project_parent_dir=tmp_path,
                  substitution_data={**git_data, **init_data},
                  skip_install=True)
    generate(project_path,
             substitution_data=git_data,
             skip_install=True)

    # Then
    project_dir = tmp_path / TEST_PROJECT_SLUG
    assert_that(call_shell("make help-git", working_dir=project_dir),
                is_(MAKE_RETURN_CODE_OK),
                "Make targets should be installed")


def test_add_gitlab_should_be_able_to_add_gitlab_ci(tmpdir):
    # Given
    project_path = tmpdir / TEST_PROJECT_SLUG
    mkdir(project_path)

    # When
    generate(project_path,
             substitution_data=GITLAB_DATA,
             skip_install=True)

    # Then
    assert_that((project_path / GITLAB_CI_FILE_NAME).exists(),
                "gitlab-ci script should be created")
