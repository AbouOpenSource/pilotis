from os import mkdir
from pathlib import Path
from subprocess import call
from unittest import mock
import pytest
import toml
import yaml
from hamcrest import assert_that, not_, has_key, contains_exactly, is_

import pilotis
from pilotis.commands.aws import generate_aws, AWS_DATA_STORAGE_DOC_SECTION, OPTION_AWS_TERRAFORM_BUCKET_NAME, OPTION_AWS_TERRAFORM_BUCKET_DIRECTORY, \
    OPTION_AWS_DATA_BUCKET_NAME
from pilotis.commands.init import OPTION_PROJECT_NAME, OPTION_PROJECT_SLUG, generate, OPTION_PYTHON_PACKAGE_NAME
from pilotis.domain.python_project_config import PYPROJECT_TOOL_SECTION_NAME, PYPROJECT_POETRY_SECTION_NAME, \
    PYPROJECT_DEPENDENCIES_SECTION_NAME, PYTHON_PROJECT_DIRECTORY_NAME, PYPROJECT_TOML_FILE_NAME, NoPyprojectException
from .test_tools import fake_pilotis_initialization, set_aws_environment_variables

TEST_PROJECT_NAME = "Pilotis Test Project"
TEST_PROJECT_SLUG = "pilotis-test-project"
TEST_PROJECT_PYTHON_PACKAGE_NAME = "pilotis_test_project"

GREP_NO_MATCHING_RETURN_CODE = 1
COMMAND_GENERIC_RETURN_CODE_OK = 0

TF_BUCKET_NAME = "tf_bucket"
DATA_BUCKET_NAME = "data_bucket"
PROJECT_PATH_ON_BUCKET = "projects"
AWS_DATA = {
    OPTION_PROJECT_SLUG: TEST_PROJECT_SLUG,
    OPTION_AWS_DATA_BUCKET_NAME: DATA_BUCKET_NAME,
    OPTION_AWS_TERRAFORM_BUCKET_NAME: TF_BUCKET_NAME,
    OPTION_AWS_TERRAFORM_BUCKET_DIRECTORY: PROJECT_PATH_ON_BUCKET
}


def test_generate_infra_should_create_infra_folder_content(tmp_path: Path):
    # Given
    project_path: Path = tmp_path / TEST_PROJECT_SLUG
    mkdir(project_path)
    fake_pilotis_initialization(project_path)

    # When
    generate_aws(project_path, substitution_data=AWS_DATA, skip_install=True)

    # Then
    assert_that((project_path / "infrastructure").exists(),
                "infrastructure folder should be created")
    created_elements_in_project_dir = [file.name
                                       for file in Path(project_path / "infrastructure").iterdir()
                                       if file.is_file()]
    created_elements_in_project_dir.sort()
    # noinspection PyTypeChecker
    assert_that(created_elements_in_project_dir, contains_exactly(
        ".gitignore",
        "backend.tf",
        "provider.tf",
        "set_aws_credentials.bash.template",
        "storage.tf",
        "versions.tf",
    ))
    _assert_jinja_tags_are_replaced(str(project_path / "infrastructure"))


def _assert_jinja_tags_are_replaced(project_dir):
    assert_that(call(f"grep -qrIl {OPTION_PROJECT_SLUG} {project_dir}", shell=True),
                is_(GREP_NO_MATCHING_RETURN_CODE),
                "Jinja project slug tags should be replaced with given value")
    assert_that(call(f"grep -qrIl {OPTION_AWS_DATA_BUCKET_NAME} {project_dir}", shell=True),
                is_(GREP_NO_MATCHING_RETURN_CODE),
                "Jinja AWS data bucket name tag should be replaced with given value")
    assert_that(call(f"grep -qrIl {OPTION_AWS_TERRAFORM_BUCKET_NAME} {project_dir}", shell=True),
                is_(GREP_NO_MATCHING_RETURN_CODE),
                "Jinja AWS TF bucket name tag should be replaced with given value")
    assert_that(call(f"grep -qrIl {OPTION_AWS_TERRAFORM_BUCKET_DIRECTORY} {project_dir}", shell=True),
                is_(GREP_NO_MATCHING_RETURN_CODE),
                "Jinja AWS TF bucket directory tag should be replaced with given value")
    assert_that(call(f"grep -qrIl {TEST_PROJECT_SLUG} {project_dir}", shell=True),
                is_(COMMAND_GENERIC_RETURN_CODE_OK),
                "Jinja project slug tags should be replaced with given value")
    assert_that(call(f"grep -rIl {DATA_BUCKET_NAME} {project_dir}", shell=True),
                is_(COMMAND_GENERIC_RETURN_CODE_OK),
                "Jinja AWS data bucket tag should be replaced with given value")
    assert_that(call(f"grep -rIl {TF_BUCKET_NAME} {project_dir}", shell=True),
                is_(COMMAND_GENERIC_RETURN_CODE_OK),
                "Jinja AWS TF bucket tag should be replaced with given value")
    assert_that(call(f"grep -rIl {PROJECT_PATH_ON_BUCKET} {project_dir}", shell=True),
                is_(COMMAND_GENERIC_RETURN_CODE_OK),
                "Jinja AWS TF bucket directory tag should be replaced with given value")


def test_generate_aws_should_add_pilotis_io_io_aws_dependency(tmp_path: Path):
    # Given
    init_data = {
        OPTION_PROJECT_NAME: TEST_PROJECT_NAME,
        OPTION_PROJECT_SLUG: TEST_PROJECT_SLUG,
        OPTION_PYTHON_PACKAGE_NAME: TEST_PROJECT_PYTHON_PACKAGE_NAME
    }
    py_project_file = tmp_path / TEST_PROJECT_SLUG / PYTHON_PROJECT_DIRECTORY_NAME / PYPROJECT_TOML_FILE_NAME

    # When
    generate(tmp_path,
             substitution_data=init_data,
             skip_install=True)

    # Then
    poetry_toml_after_aws_generation = toml.load(py_project_file)
    pyproject_dependencies_after_aws_generation = poetry_toml_after_aws_generation[PYPROJECT_TOOL_SECTION_NAME][PYPROJECT_POETRY_SECTION_NAME]\
        [PYPROJECT_DEPENDENCIES_SECTION_NAME]
    assert_that(pyproject_dependencies_after_aws_generation, has_key('pilotis-io'))
    assert_that(pyproject_dependencies_after_aws_generation, not_(has_key('pilotis-io-aws')))
    assert_that(pyproject_dependencies_after_aws_generation, not_(has_key('boto3')))
    assert_that(pyproject_dependencies_after_aws_generation, not_(has_key('s3fs')))

    # When
    fake_pilotis_initialization(tmp_path / TEST_PROJECT_SLUG)
    project_path: Path = tmp_path / TEST_PROJECT_SLUG
    generate_aws(project_path, substitution_data=AWS_DATA, skip_install=True)

    # Then
    poetry_toml_after_aws_generation = toml.load(py_project_file)

    pyproject_dependencies_after_aws_generation = poetry_toml_after_aws_generation\
        [PYPROJECT_TOOL_SECTION_NAME][PYPROJECT_POETRY_SECTION_NAME][PYPROJECT_DEPENDENCIES_SECTION_NAME]
    assert_that(pyproject_dependencies_after_aws_generation, has_key('pilotis-io'))
    assert_that(pyproject_dependencies_after_aws_generation, has_key('pilotis-io-aws'))
    assert_that(pyproject_dependencies_after_aws_generation, has_key('boto3'))
    assert_that(pyproject_dependencies_after_aws_generation, has_key('s3fs'))


def test_generate_aws_should_throw_an_error_if_target_directory_is_not_a_poetry_directory(tmp_path: Path):
    # Given
    project_path: Path = tmp_path / TEST_PROJECT_SLUG
    mkdir(project_path)

    # When / Then
    with pytest.raises(NoPyprojectException):
        generate_aws(project_path, substitution_data=AWS_DATA, skip_install=True)


def test_pilotis_aws_should_run_terraform_initialization(tmp_path, mocker):
    # Given
    set_aws_environment_variables()
    mocker.patch("pilotis.commands.aws.call_shell")
    fake_pilotis_initialization(tmp_path)

    # When
    generate_aws(tmp_path, substitution_data=AWS_DATA, skip_install=False)

    # Then
    pilotis.commands.aws.call_shell.assert_has_calls([
        mock.call("terraform init", tmp_path / "infrastructure"),
        mock.call("terraform apply -auto-approve", tmp_path / "infrastructure"),
    ])


def test_pilotis_aws_should_not_run_terraform_initialization_given_skip_install(tmp_path, mocker):
    # Given
    set_aws_environment_variables()
    mocker.patch("pilotis.commands.aws.call_shell")
    fake_pilotis_initialization(tmp_path)

    # When
    generate_aws(project_dir=tmp_path, substitution_data=AWS_DATA, skip_install=True)

    # Then
    pilotis.commands.aws.call_shell.assert_not_called()


def test_generate_aws_should_add_aws_doc(tmp_path: Path):
    # Given
    fake_pilotis_initialization(tmp_path)

    # When
    generate_aws(tmp_path, substitution_data=AWS_DATA, skip_install=True)

    # Then
    python_project_path = tmp_path / PYTHON_PROJECT_DIRECTORY_NAME
    mkdocs_config_file = python_project_path / "mkdocs.yml"
    with open(mkdocs_config_file, 'r') as readme_file_stream:
        mkdocs_config_content_post_aws = yaml.load(readme_file_stream)
    assert_that(AWS_DATA_STORAGE_DOC_SECTION in mkdocs_config_content_post_aws['nav'])

    aws_docs_page = python_project_path / "docs" / "aws-data-storage.md"
    assert_that(aws_docs_page.exists())

