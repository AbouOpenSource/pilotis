import os
from pathlib import Path
from typing import Set
from unittest.mock import call

import pytest
from click.testing import CliRunner
from hamcrest import assert_that, contains_string, equal_to, not_, matches_regexp
from pytest_mock import MockerFixture

from pilotis.cli import main
from .test_aws import AWS_DATA
from .test_tools import fake_pilotis_initialization, set_aws_environment_variables, ENV_AWS_ACCESS_KEY_ID, \
    ENV_AWS_SECRET_ACCESS_KEY, ENV_AWS_DEFAULT_REGION

MISSING_ENV_VARIABLE_ERROR_MESSAGE = "Missing AWS environment variable"
NOT_A_PILOTIS_DIRECTORY_ERROR_MESSAGE = "This command should be run in a pilotis project directory"

COMMAND_SUCCESS_CODE = 0


@pytest.fixture
def runner():
    return CliRunner()


def test_pilotis_help_should_be_available(runner):
    result = runner.invoke(main, ['--help'])
    assert_that(result.exit_code, equal_to(COMMAND_SUCCESS_CODE), "Exit code")


def test_pilotis_help_display_init_command_help(runner):
    result = runner.invoke(main, ['--help'])
    assert_that(result.output, matches_regexp("init *Initialize project: generate base folders & files."))


def test_pilotis_help_display_git_command_help(runner):
    result = runner.invoke(main, ['--help'])
    assert_that(result.output, matches_regexp("git *Version with Git & create remote."))


def test_pilotis_help_display_aws_command_help(runner):
    result = runner.invoke(main, ['--help'])
    assert_that(result.output, matches_regexp("aws *Use AWS as a technical backend"))


def test_pilotis_aws_help_should_display_aws_command_long_help(runner):
    result = runner.invoke(main, ['aws', '--help'])
    assert_that(result.output, contains_string("Use AWS as a technical backend"))
    assert_that(result.output, matches_regexp("--project-dir *DIRECTORY *Directory containing the project"))
    assert_that(result.output, matches_regexp("--skip-install *Skip post installation scripts"))


def test_pilotis_help_display_main_usage(runner):
    result = runner.invoke(main, ['--help'])
    assert_that(result.output, contains_string("Usage: pilotis [OPTIONS] COMMAND [ARGS]..."))


def test_pilotis_git_should_fail_if_not_in_an_pilotis_project_directory(tmp_path):
    # When
    result = CliRunner().invoke(main, ['git', f'--project-dir={str(tmp_path)}'], catch_exceptions=False,
                                input="\n")

    # Then
    assert_that(result.exit_code, not_(equal_to(COMMAND_SUCCESS_CODE)))
    assert_that(result.stdout, contains_string(NOT_A_PILOTIS_DIRECTORY_ERROR_MESSAGE))


def test_pilotis_aws_should_fail_if_missing_aws_secret_key(tmp_path):
    # Given
    set_aws_environment_variables()
    os.environ.pop(ENV_AWS_SECRET_ACCESS_KEY)
    fake_pilotis_initialization(tmp_path)

    # When
    result = CliRunner().invoke(main, ['aws', f'--project-dir={str(tmp_path)}', '--skip-install'],
                                catch_exceptions=False)

    # Then
    assert_that(result.exit_code, not_(equal_to(COMMAND_SUCCESS_CODE)))
    env_vars = {ENV_AWS_ACCESS_KEY_ID, ENV_AWS_DEFAULT_REGION, ENV_AWS_SECRET_ACCESS_KEY}
    _contains_all_strings(result.stdout, {MISSING_ENV_VARIABLE_ERROR_MESSAGE}.union(env_vars))


def test_pilotis_aws_should_fail_if_missing_aws_region(tmp_path):
    # Given
    set_aws_environment_variables()
    os.environ.pop(ENV_AWS_DEFAULT_REGION)
    fake_pilotis_initialization(tmp_path)

    # When
    result = CliRunner().invoke(main, ['aws', f'--project-dir={str(tmp_path)}', '--skip-install'],
                                catch_exceptions=False)

    # Then
    assert_that(result.exit_code, not_(equal_to(COMMAND_SUCCESS_CODE)))
    env_vars = {ENV_AWS_ACCESS_KEY_ID, ENV_AWS_DEFAULT_REGION, ENV_AWS_SECRET_ACCESS_KEY}
    _contains_all_strings(result.stdout, {MISSING_ENV_VARIABLE_ERROR_MESSAGE}.union(env_vars))


def test_pilotis_aws_should_fail_if_not_in_an_pilotis_project_directory(tmp_path):
    # Given
    set_aws_environment_variables()

    # When
    result = CliRunner().invoke(main, ['aws', f'--project-dir={str(tmp_path)}', '--skip-install'],
                                catch_exceptions=False)

    # Then
    assert_that(result.exit_code, not_(equal_to(COMMAND_SUCCESS_CODE)))
    assert_that(result.stdout, contains_string(NOT_A_PILOTIS_DIRECTORY_ERROR_MESSAGE))


def test_pilotis_aws_should_inform_the_user_about_project_name(tmp_path, mocker, runner):
    # Given
    set_aws_environment_variables()
    mocker.patch("pilotis.commands.aws.call_shell")
    project_slug = 'project-slug'

    # When
    result = runner.invoke(main, ['aws', f'--project-dir={str(tmp_path / project_slug)}', '--skip-install'],
                           catch_exceptions=False)

    # Then
    assert_that(result.output, contains_string(f"Generating AWS on {project_slug}"))


def test_pilotis_aws_should_inform_the_user_about_project_name_given_dot_as_project_dir(tmp_path, mocker, runner,
                                                                                         monkeypatch):
    # Given
    set_aws_environment_variables()
    mocker.patch("pilotis.commands.aws.call_shell")
    project_slug = 'test-project'
    project_path = tmp_path / project_slug
    project_path.mkdir()
    monkeypatch.chdir(project_path)

    # When
    result = runner.invoke(main, ['aws', '--project-dir=.', '--skip-install'], catch_exceptions=False)

    # Then
    assert_that(result.output, contains_string(f"Generating AWS on {project_slug}"))


def test_pilotis_aws_should_fail_if_missing_aws_access_key(tmp_path):
    # Given
    set_aws_environment_variables()
    os.environ.pop(ENV_AWS_ACCESS_KEY_ID)
    fake_pilotis_initialization(tmp_path)

    # When
    result = CliRunner().invoke(main, ['aws', f'--project-dir={str(tmp_path)}', '--skip-install'],
                                catch_exceptions=False)

    # Then
    assert_that(result.exit_code, not_(equal_to(COMMAND_SUCCESS_CODE)))
    env_vars = {ENV_AWS_ACCESS_KEY_ID, ENV_AWS_DEFAULT_REGION, ENV_AWS_SECRET_ACCESS_KEY}
    _contains_all_strings(result.stdout, {MISSING_ENV_VARIABLE_ERROR_MESSAGE}.union(env_vars))


def _contains_all_strings(string_to_check: str, search_strings: Set[str]) -> None:
    [assert_that(string_to_check, contains_string(search_string)) for search_string in search_strings]


def test_pilotis_aws_should_parse_parameters(tmp_path, mocker: MockerFixture):
    # Given
    mocker.patch("pilotis.commands.aws._aws_substitution_data", new=lambda _: AWS_DATA)
    generate_aws_function_stub = mocker.patch("pilotis.commands.aws.generate_aws")
    fake_pilotis_initialization(tmp_path)
    set_aws_environment_variables()

    # When
    result = CliRunner().invoke(main, ['aws', f'--project-dir={str(tmp_path)}', '--skip-install'])

    # Then
    assert_that(result.exit_code, equal_to(COMMAND_SUCCESS_CODE))
    generate_aws_function_stub.assert_has_calls([
        call(tmp_path, AWS_DATA, True),
    ])


def test_pilotis_aws_should_default_skip_install_to_false(tmp_path, mocker: MockerFixture):
    # Given
    mocker.patch("pilotis.commands.aws._aws_substitution_data", new=lambda _: AWS_DATA)
    generate_aws_function_stub = mocker.patch("pilotis.commands.aws.generate_aws")
    fake_pilotis_initialization(tmp_path)
    set_aws_environment_variables()

    # When
    result = CliRunner().invoke(main, ['aws', f'--project-dir={str(tmp_path)}'])

    # Then
    assert_that(result.exit_code, equal_to(COMMAND_SUCCESS_CODE))
    generate_aws_function_stub.assert_has_calls([
        call(tmp_path, AWS_DATA, False),
    ])


def test_pilotis_aws_should_default_project_dir_to_current_dir(tmp_path, mocker: MockerFixture):
    # Given
    mocker.patch("pilotis.commands.aws._aws_substitution_data", new=lambda _: AWS_DATA)
    generate_aws_function_stub = mocker.patch("pilotis.commands.aws.generate_aws")
    set_aws_environment_variables()
    cli_runner = CliRunner()

    with cli_runner.isolated_filesystem() as dir_name:
        dir_path = Path(dir_name)
        fake_pilotis_initialization(dir_path)

        # When
        result = cli_runner.invoke(main, ['aws', '--skip-install'])

        # Then
        assert_that(result.exit_code, equal_to(COMMAND_SUCCESS_CODE))
        generate_aws_function_stub.assert_has_calls([
            call(dir_path, AWS_DATA, True),
        ])
