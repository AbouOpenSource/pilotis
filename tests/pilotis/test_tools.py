import os
import sys
from io import StringIO
from pathlib import Path

import yaml

from pilotis.commands.aws import MKDOCS_CONFIG_FILE_NAME
from pilotis.domain.python_project_config import PYTHON_PROJECT_DIRECTORY_NAME, PYPROJECT_TOML_FILE_NAME

PILOTIS_CONFIG_FILE_NAME = "pilotis.config"
ENV_AWS_ACCESS_KEY_ID = "AWS_ACCESS_KEY_ID"
ENV_AWS_SECRET_ACCESS_KEY = "AWS_SECRET_ACCESS_KEY"
ENV_AWS_DEFAULT_REGION = "AWS_DEFAULT_REGION"


def fake_pilotis_initialization(tmp_path: Path):
    (tmp_path / PILOTIS_CONFIG_FILE_NAME).touch()

    python_project_path = tmp_path / PYTHON_PROJECT_DIRECTORY_NAME
    python_project_path.mkdir(exist_ok=True)

    pyproject_file_path = python_project_path / PYPROJECT_TOML_FILE_NAME

    if not (pyproject_file_path.exists()):
        pyproject_file_path.touch()
        with open(pyproject_file_path, 'w') as file_stream:
            file_stream.write('[tool.poetry.dependencies]')

    mkdoc_file_path = python_project_path / MKDOCS_CONFIG_FILE_NAME

    if not mkdoc_file_path.exists():
        mkdoc_file_path.touch()
        default_mkdoc_config = {
            'nav': []
        }
        with mkdoc_file_path.open('w') as mkdoc_file_handler:
            yaml.dump(default_mkdoc_config, mkdoc_file_handler)


def set_aws_environment_variables():
    os.environ[ENV_AWS_ACCESS_KEY_ID] = "acces key id"
    os.environ[ENV_AWS_DEFAULT_REGION] = "eu-west"
    os.environ[ENV_AWS_SECRET_ACCESS_KEY] = "very secret"


class Capturing(list):
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self

    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        del self._stringio
        sys.stdout = self._stdout
