# file modified from https://gist.github.com/lumengxi/0ae4645124cd4066f676
.PHONY: *

#################################################################
# Shared variables
#################################################################

PACKAGE_DIR=pilotis_io

define BROWSER_PYSCRIPT
import os, webbrowser, sys
try:
	from urllib import pathname2url
except:
	from urllib.request import pathname2url

webbrowser.open("file://" + pathname2url(os.path.abspath(sys.argv[1])))
endef
export BROWSER_PYSCRIPT
BROWSER := python -c "$$BROWSER_PYSCRIPT"

#################################################################
# Shared functions
#################################################################

# Check that given variables are set and all have non-empty values,
# die with an error otherwise.
#
# Params:
#   1. Variable name(s) to test.
#   2. (optional) Error message to print.
# Details at https://stackoverflow.com/a/10858332/4374048
check_defined = \
    $(strip $(foreach 1,$1, \
        $(call __check_defined,$1,$(strip $(value 2)))))
__check_defined = \
    $(if $(value $1),, \
      $(error Undefined $1$(if $2, ($2)): please pass as argument (see help target)))

#################################################################
# setting dependencies for automation (tox, cicd)
#################################################################

install-test-dependencies :
	poetry install --no-dev --no-root -E test

#################################################################
# setting up ci-cd env
#################################################################

setup-cicd-python3:
	update-alternatives --install /usr/bin/python python /usr/bin/python3 1
	curl -sSL  https://bootstrap.pypa.io/get-pip.py | python

setup-cicd-poetry:
	curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python
	. $(HOME)/.poetry/env && poetry config virtualenvs.create false
	echo "WARNING: you still need to source $$HOME/.poetry/env to access poetry's executable"

setup-cicd-git:
	git config --global user.email "cd-pipeline@ekinox.io"
	git config --global user.name "CICD Pipeline"

setup-cicd-test-stage: setup-cicd-poetry setup-cicd-git

test:
	poetry run pytest
