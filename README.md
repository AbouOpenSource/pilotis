[![Pipeline Status](https://gitlab.com/ekinox-io/ekinox-libraries/pilotis/badges/master/pipeline.svg)](https://gitlab.com/ekinox-io/ekinox-libraries/pilotis/pipelines)

# Pilotis

`pilotis` is the core package of an opinionated framework aiming at facilitating and accelerating
the creation of Data Science python applications, from prototyping till usage in production. More
specifically, `pilotis` allows you to :

- create a new python project from scratch, with a source and test package, a notebook repository, a data folder
  organization and a poetry virtual environment
- create a gitlab repository for your project with a preconfigured CI pipeline if needed
- provision an infrastructure on AWS if needed, with the same tools as in local
- access [a library](https://gitlab.com/ekinox-io/ekinox-libraries/pilotis-io) allowing you to easily load
  your data from S3 or local storage, with a common API

# Requirements

- Pyenv
- Poetry

# Install Pilotis

From sources

```bash
pip install <pilotis_source_directory>
```

# Initialize a new project

Execute the following script :

```bash
pilotis init --project-parent-dir <SOME_PROJECT_DIR>
```

# Initialize version control

Then, if you want to use some version control, run :

```bash
pilotis git --project-dir <SOME_PROJECT_DIR>
```

# Data storage

If you want to store your data in AWS, we provide a command to easily create the bucket.

## Requirements

- [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html)
- [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli)

## Initialization steps

First, grab your AWS secret information (access key id, secret access key, default region) and set the following
environment variables :

```bash
export AWS_ACCESS_KEY_ID=<your access key>
export AWS_DEFAULT_REGION=<your default region>
export AWS_SECRET_ACCESS_KEY=<your secret key>
```

Then, you can prepare a S3 bucket to share the data with all project members:

```bash
pilotis aws --project-dir=<project dir path>
```

If you just want to generate the Terraform scripts without launching them, you can add the `--skip-install` flag:

```bash
pilotis aws --skip-install --<project dir path>
```

Finally, get the data in the `workdir/landing/raw` repository, and run the following command to synchronize it to s3

```bash
make sync-raw-data-local-to-s3
```

## Delete cloud infrastructure

Destroy everything (bucket + bucket policy + everything you have created after):

```bash
make destroy-aws-infrastructure
```

# Testing

In order to test the behavior of `pilotis`, you can:

- launch unit tests with the command `poetry run pytest`
- launch an end-to-end test:
    1. Run `docker/build.bash`
    1. Run the command displayed on the standard output at the end of the build

# Troubleshooting

## poetry could not be installed even if removed from path

Context :

- Poetry removed from path
- Trying to install poetry
  using `curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -`

Observed error :

- Log saying poetry is already installed in the latest version event if this is not the case

How to fix :

```bash
mv ~/.poetry ~/.poetry.backup
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
```

## poetry issues an error about cleo

Usually, it happens when poetry has been upgraded from a version not compatible with the one that replaces it.

The solution is to uninstall and reinstall poetry :

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python - --uninstall
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
```
